<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

  /**
   * Index Page for this controller.
   *
   * Maps to the following URL
   * 		http://example.com/index.php/welcome
   *	- or -
   * 		http://example.com/index.php/welcome/index
   *	- or -
   * Since this controller is set as the default controller in
   * config/routes.php, it's displayed at http://example.com/
   *
   * So any other public methods not prefixed with an underscore will
   * map to /index.php/welcome/<method_name>
   * @see http://codeigniter.com/user_guide/general/urls.html
   */
  public function index()
  {
    $this->load->view('index');
  }

  public function page()
  {
    $page = $this->uri->rsegment(3);
    $data = "";
    if(strcmp($page, "about") == 0) {
      $data['page_title'] = "About Us";
      $data['meta_desc'] = "About Us";
    }elseif(strcmp($page, "mission") == 0) {
      $data['page_title'] = "Our Mission";
      $data['meta_desc'] = "Our Mission";
    } elseif(strcmp($page, "events") == 0) {
      $data['page_title'] = "Our Events";
      $data['meta_desc'] = "Our events";
    }elseif(strcmp($page, "donate") == 0) {
      $data['page_title'] = "Donate";
      $data['meta_desc'] = "Donate";
    } elseif(strcmp($page, "news") == 0) {
      $data['page_title'] = "SANA in the News";
      $data['meta_desc'] = "SANA in the news";
    } elseif(strcmp($page, "contactus") == 0) {
      $data['page_title'] = "Contact Us";
      $data['meta_desc'] = "Contact Us";
    }

    $this->load->view($page, $data);
  }
  public function send_message()
  {
    $name = $this->input->post('name');
    $email = $this->input->post('email');
    $subject = $this->input->post('subject');
    $message = $this->input->post('message');
    if (($name=="")||($email=="")||($message=="")) 
    { 
      echo "All fields are required, please fill <a href=\"\">the form</a> again."; 
      $this->load->view('contactus');
      return;
    } 
    else{
      $from="From: $name<$email>\r\nReturn-path: $email"; 
      $subject=$subject; 
      $mail = mail("info@sanausa.org", $subject, $message, $from); 
      if($mail) {
        echo "Email sent!";
      } else {
        echo "Error sending email. Please email info@sanausa.org directly";
        $error = error_get_last();
        echo $error;
      }

    }
    $this->load->view('contactus');
  }

  public function paypal() {
    $url = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=RDM426L4U5FGS";
    $this->load->helper('url');
    redirect($url, 'refresh');
  }

  public function donors() {
    $this->load->library('paypal');
    $transaction_list=$this->paypal->get_transaction_list();
    $data['transaction_list'] = $transaction_list;
    $data['page_title'] = 'Donors List';
    $data['meta_desc'] = 'Donors List';
    $this->load->view('paypal_donors', $data);
  }
}
