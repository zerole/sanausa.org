<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Paypal 
{

  function Paypal() 
  {
  }

  public function get_transaction_list() {
    $ini_array = parse_ini_file("config.ini");
    $info = 'USER='
      .$ini_array['USER']
      .'&PWD='
      .$ini_array['PWD']
      .'&SIGNATURE='
      .$ini_array['SIGNATURE']
      .'&METHOD=TransactionSearch'
      .'&TRANSACTIONCLASS=RECEIVED'
      .'&STARTDATE=2015-01-08T05:38:48Z'
      .'&ENDDATE=2016-12-31T05:38:48Z'
      .'&VERSION=94';

    $curl = curl_init('https://api-3t.paypal.com/nvp');
    curl_setopt($curl, CURLOPT_FAILONERROR, true);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

    curl_setopt($curl, CURLOPT_POSTFIELDS,  $info);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_POST, 1);

    $result = curl_exec($curl);

    # Bust the string up into an array by the ampersand (&)
    # # You could also use parse_str(), but it would most likely limit out
    $result = explode("&", $result);

    # # Loop through the new array and further bust up each element by the equal sign (=)
    # # and then create a new array with the left side of the equal sign as the key and the right side of the equal sign as the value
    foreach($result as $value){
      $value = explode("=", $value);
      $temp[$value[0]] = $value[1];
    }
    # At the time of writing this code, there were 11 different types of responses that were returned for each record
    # There may only be 10 records returned, but there will be 110 keys in our array which contain all the different pieces of information for each record
    # Now create a 2 dimensional array with all the information for each record together
    for($i=0; $i<floor(count($temp)/11); $i++){
      if(urldecode($temp["L_EMAIL".$i]) == "info@goalazo.com" and urldecode($temp["L_AMT".$i]) == 146) {
        // this amount was transferred from Sudarshan's paypal account. so dont calculate twice.
        continue;
      }
      $returned_array[$i] = array(
        "timestamp"         =>    $this->date_format(urldecode($temp["L_TIMESTAMP".$i])),
        "timezone"          =>    urldecode($temp["L_TIMEZONE".$i]),
        "type"              =>    urldecode($temp["L_TYPE".$i]),
        "email"             =>    urldecode($temp["L_EMAIL".$i]),
        "name"              =>    urldecode($temp["L_NAME".$i]),
        "transaction_id"    =>    urldecode($temp["L_TRANSACTIONID".$i]),
        "status"            =>    urldecode($temp["L_STATUS".$i]),
        "amt"               =>    urldecode($temp["L_AMT".$i]),
        "currency_code"     =>    urldecode($temp["L_CURRENCYCODE".$i]),
        "fee_amount"        =>    urldecode($temp["L_FEEAMT".$i]),
        "net_amount"        =>    urldecode($temp["L_NETAMT".$i]));
    }
    #var_dump($returned_array);
    return $returned_array;
  }
  function date_format($timestamp) {
    $date = new DateTime($timestamp);
    return $date->format('M d, Y');
  }
}

