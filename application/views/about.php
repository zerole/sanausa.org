<?php include('header.php'); ?>
<!--===========================================upcoming event events==============================================-->
      <section class="staff-heading">
        <div class="container clearfix">
          <div class="pull-left">
              <h4>ABOUT</h4>
            </div>
            <div class="pull-right">
              <a href="">Home <i class="fa fa-angle-double-right"></i></a>
                <span>About</span>
            </div>
        
        </div>
      
      
      </section>


<!--===========================================upcoming events==============================================--> 
     <section class="even">
     
      <div class="container">
          <div class="span1"></div>
          <div class="span10 pull-left">
          <p>San Antonio Nepali Association (SANA) is a non-profit organization which serves as an official forum for the Nepali speaking community of San Antonio, Texas, USA as well as the medium for communication and cultural exchange between the Nepali speaking community and well-wishers outside of the community.</p>
          <p>Over the past 15 years, the Nepali population in San Antonio has steadily increased to over 5,000. Although the Nepali community has been active during these years there was no official agenda for the long term growth and development or an official organization to represent the Nepali speaking community of San Antonio. As a result, the community became disconnected and it wasn’t able to keep up with the needs of the growing population. This also resulted in a lack of fostering environment for the newer generation leading to a gradual loss of Nepali language, culture, heritage among the newer generation. So it is critical that the whole Nepali community unite and work proactively if we want to overcome these problems and want our community to have a better, prosperous future. </p>
          <p>San Antonio Nepali Association (SANA) was created recently as an official forum for the Nepali speaking community of San Antonio so that all the Nepali speaking people in San Antonio can be better connected and work together to help preserve our culture, heritage for the future generations and to help develop our community. Nepali people are known for their hospitality and harmonious coexistence without discrimination based on race, religion or beliefs. Conforming to these beliefs, SANA will not discriminate against anyone and would work with the local San Antonio community to help improve the society. SANA will hold cultural programmes and activities for the general public so the public can be knowledgeable about who we are as a community and our culture.</p>
          </div>      
          <div class="span1"></div>
</div>
    
    
    </section>
<?php include('footer.php'); ?>
