<?php include('header.php'); ?>

<!--===========================================upcoming event events==============================================-->
<section class="staff-heading">
  <div class="container clearfix">
    <div class="pull-left">
      <h4>DONATE</h4>
    </div>
    <div class="pull-right">
      <a href="/">Home <i class="fa fa-angle-double-right"></i></a>
      <span>Donate</span>
    </div>

  </div>
</section>
<!--===========================================upcoming events==============================================--> 
<section class="even">

  <div class="container">
    <div class="span12 clearfix">
      <div class="span1"></div>
      <div class="span10">
        <div class="pull-left span8 event-title">
          <div class="span8 pull-left"><p>
            <p>Nepal was hit by a devastating earthquake of 7.8 magnitude on April 25, 2015. The earthquake was followed by more than 100 aftershocks, some bigger than 5 and 6 Magnitude. The country was in terror for over a week, and continues to be in the ruins. Thousands died. Missions are affected. Billions of dollars worth of property and infrastructures were destroyed.</p> 
            <p>SANA is raising money for for the relief and rebuilding of Nepal. 100% of the donations will go the victims in Nepal, either for relief or for rebuilding the infrastructures. Please donate. </p><p>Please donate. Every penny counts.</p>
            </br>
          </div>
        </div>
      </div>

      <div class="span1"></div>
    </div>
    <div class="span12 clearfix">
      <div class="span1"></div>
      <div class="span10">
        <h3>Using Paypal (Use Credit Card or Paypal Account)</h3>
        <div class="span8">
        <p>
        <a href="paypal" target="window"><img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif"></a>
        </p>
        <br/><br/>
        <p>
        <h3>Or <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=RDM426L4U5FGS" target="window">Click here</a><h3/>                               
        </p>               
        </div>           
      </div>

      <div class="span1"></div>
    </div>
    <div class="span12 clearfix">
      <div class="span1"></div>
      <div class="span10">
          <h3>Pay by Check</h3>
          <div class="span8"><p>Send check addressed to <b>San Antonio Nepalese Association</b> to the following address:</p>
            <p><b>7923 Coolspring Drive, San Antonio, Texas 78254</b></p>

          </div>
        </div>
        <div class="span1"></div>
      </div>

    </div>


  </section>
  <?php include('footer.php'); ?>
