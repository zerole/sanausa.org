        <!--===========================================FOOTER==============================================--> 
    <section class="footer">
          <div class="container">
              <div class="span3 coloum1">
          <div class="footer-logo">
          <a href="/"><img src="<?php echo base_url('assets/images/sanausa_logo.png') ?>" alt="logo" height="45px" width="117px"/></a>
                  </div>
          
          <p>San Antonio<br/>
Texas, USA</p>
                    <p><i class="fa fa-envelope-o"></i>Email: info@sanausa.org</p>
                </div>
                
                <div class="span3 coloum4">
                  <h4>MORE ABOUT US</h4>
                  <p>SANA (San Antonio Nepalese Association) is the home of Nepali people living in San Antonio, Texas.</p>
                    <a href="about">Read more..</a>
                </div>
                <div class="span6">
                  <a class="twitter-timeline" href="https://twitter.com/hashtag/NepalEarthquake" data-widget-id="594187611318538240">#NepalEarthquake Tweets</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
                </div>
            
            </div>
          <p class="copy">©2015 All rights reserved.</p>
        
        </section>
    
    
    
    
    
    
    
<script src="<?php echo base_url('assets/js/jquery.js') ?>" type="text/javascript" charset="utf-8"></script> 
    <!--==========================================SLIDER JS=============================================-->
     
    <script src="<?php echo base_url('assets/js/min/modernizr-custom-v2.7.1.min.js') ?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/min/jquery-finger-v0.1.0.min.js') ?>" type="text/javascript"></script>
    <script>
    $(document).ready(function(){
      
      $('.flicker-example').flicker();
    });
    </script>
      <!-- the jScrollPane script -->
<script type="text/javascript" src="<?php echo base_url('assets/js/migrate.js') ?>"></script> 
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.mousewheel.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.contentcarousel.js') ?>"></script>
    <script type="text/javascript">
      $('#ca-container').contentcarousel();
    </script>
           <!--==========================================FANCY BOX JS=============================================-->
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.fancybox.pack.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/owl.carousel.js') ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/custom.js') ?>"></script>   
     <!--==========================================BOOSTRAP JS=============================================-->
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.js') ?>"></script>
       
    
    <script src="<?php echo base_url('assets/js/tesi.js') ?>" type="text/javascript"></script>
      
        <!--============================================time counter jquery========================================-->
      <script src="<?php echo base_url('assets/js/jquery.countdown.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/function.js') ?>"></script>
          
    <!--============================================Progress bar========================================-->     
<script type="text/javascript" src="<?php echo base_url('assets/js/goalProgress.js') ?>"></script>
     <script src="<?php echo base_url('assets/js/progressjs.js') ?>"></script>     
       <!--==========================================horizontal JS=============================================-->
<script type="text/javascript" src="<?php echo base_url('assets/js/horizental/modernizr.js') ?>"></script>
      <script type='text/javascript' src="<?php echo base_url('assets/js/horizental/plugins.js') ?>"></script>
    <script type='text/javascript' src="<?php echo base_url('assets/js/horizental/sly.js') ?>"></script>
        <script type='text/javascript' src="<?php echo base_url('assets/js/horizental/horizontal.js') ?>"></script>
      
        <!--==========================================sticky navbar js=============================================-->
    <script type="text/javascript" src="<?php echo base_url('assets/js/sticky.js') ?>"></script>
      
       <script type="text/javascript" src="<?php echo base_url('assets/js/cbpQTRotator.min.js') ?>"></script>
       <script type="text/javascript" src="<?php echo base_url('assets/js/mypassion.js') ?>"></script> 
      
      
    
         
      
        
      
      
           
             
<!-- Start of StatCounter Code for Default Guide -->
<script type="text/javascript">
var sc_project=10417564; 
var sc_invisible=1; 
var sc_security="c384d7d5"; 
var scJsHost = (("https:" == document.location.protocol) ?
"https://secure." : "http://www.");
document.write("<sc"+"ript type='text/javascript' src='" +
scJsHost+
"statcounter.com/counter/counter.js'></"+"script>");
</script>
<noscript><div class="statcounter"><a title="shopify
analytics" href="http://statcounter.com/shopify/"
target="_blank"><img class="statcounter"
src="http://c.statcounter.com/10417564/0/c384d7d5/1/"
alt="shopify analytics"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->

</body>
</html>
