<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="user-scalable = yes" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title><?php echo isset($page_title) ? $page_title.' : SANAUSA.ORG' : DEFAULT_TITLE; ?></title>
<meta name="google-site-verification" content="anfFgW6YhRcQEMTisnjz0Lp0GeTHm-GXAnQ7mGUA3dg" />
<meta name="description" content="<?php echo isset($meta_desc) ? $meta_desc : DEFAULT_META_DESC; ?>" />
<meta name="keywords" content="<?php echo isset($meta_keywords) ? $meta_keywords : DEFAULT_META_KEYWORDS; ?>" />

 <!--==========================================BOOSTRAPE CSS=============================================-->
 <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-combined.min.css') ?>"/>    
 <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css') ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-responsive.css') ?>"/>


 <!--==========================================FONT AWOSOME=============================================-->
 <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.css') ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.min.css') ?>"/>

 <!--==========================================CUSTOM CSS=============================================-->
 <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/style.css')?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/color.css')?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/reset.css')?>"/>
<link rel="stylesheet" href="<?php echo base_url('assets/css/horizontal.css')?>"/>


<!--==========================================GOOGLE FONTS=============================================-->
<link href='http://fonts.googleapis.com/css?family=Roboto:900,400' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,800,900' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,700,800' rel='stylesheet' type='text/css'>
 

<link rel="icon" type="image/png" href="<?php echo base_url('assets/images/favicon.ico') ?>">

<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?2zXsK8zCCxqMLZsJzjcjYEqk3ho7NkDe";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->

</head>

<body>
   <!--==========================================TOP NAV=============================================-->
  <section class="top-social">
      <div class="container top-nav clearfix">
          <div class="span4 address pull-left">
              <ul>
                  <li><i class="fa fa-envelope-o"></i>
</li>
                    <li class="addrs">info@sanausa.org </li>
                </ul>
            </div>
            <div class="span4 social-icon pull-right">
              <ul>
                    <li><a href="https://www.facebook.com/groups/884391808287938/" target="window"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://twitter.com/search?f=realtime&q=%23nepal&src=typd" target="window"><i class="fa fa-twitter"></i></a></li>
          
          
                    
                </ul>
            </div>
        </div>
    </section>
  
       <!--===========================================MAIN NAV==============================================-->
  <section class="main-nav"> 
    <nav>
      <div class="main-nav bottom-nav" >
        <div class="navbar navbar-inverse clearfix">
             
          <div class="navbar-inner">
              
            <div class="container">
                
              <div class="logo pull-left">
              <a href="/"><img src="<?php echo base_url('assets/images/sanausa_logo.png') ?>"  alt="logo" width="117px" height="45px"/></a>
                
              </div>
                 <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  </button>
              <div class="nav-collapse collapse">
                <ul class="nav bottom pull-right">
                   <li id="home"><a href="/">Home</a>
                  </li>
                  <li><a href="about">About</a></li>
                 
                  
                 <li><a href="mission">Mission</a> </li>
                 
                  <li><a href="events">Events</a></li>
                 <li id="home"><a href="donate">Donate</a>
                  <ul class="dropdown-menu pull-left" role="menu">
                    <li><a href="donors">Donors</a></li>
                  </ul> 
                  </li>
                  <li><a href="news">News</a></li>
<li><a href="meetings">Meetings</a></li>
                  <li><a href="contactus">Contact Us</a></li>
                 </li> 
                 
                </ul>
              </div><!--/.nav-collapse -->
            </div>
          </div>
        </div>
      </div>
    </nav>  
   </section>
    <!--===========================================mobile navbar==============================================-->
   <section class="mobile-nav-bar">
    <nav>
      <div class="navbar  visible-phone ">
        <div class="navbar-inner clearfix">
          <div class="container">   
            <div class="mobile-logo pull-left">
            <a href=""><img src="<?php echo base_url('assets/images/sanausa_logo.png') ?>"  alt="logo"/></a>
            </div>
    <!--Button to collapse the Navigation-->
            <button type="button" class="btn pull-right mobile-button" data-toggle="collapse" data-target=".mobile-nav">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
    
            <div class="nav-collapse mobile-nav">
              <ul class="nav bottom pull-right mobile-drope">
                  <li class="mobile-drop pull-left"><a href="/">Home</a>
                    </li>
                  <li><a href="about">About</a></li>
                 
                  
                 <li><a href="mission">Mission</a> </li>
                <li><a href="events">Events</a></li>        
                 
                  <li><a href="donate">donate</a>
                    </li>
                  <li><a href="contactus">Contact Us</a></li>
                  
                  </ul>
              </ul>
            </div>
      <!--/.nav-collapse --> 
          </div>
        </div>
      </div>
    </nav>  
  </section>
