<?php include('header.php'); ?>

<!--===========================================upcoming event events==============================================-->
      <section class="demo-main">
        <div class="container clearfix">
              <h1>San Antonio <span class="demo-color">Nepalese</span> Association</h1> 
        </div>
      </section>

<!--===========================================main slider==============================================-->
<div class="flicker-example" data-block-text="false">
    <ul>
      
    <li data-background="<?php echo base_url('assets/img/nepal_slider4.jpg') ?>">
        <div class="flick-title"><span class="color">7.8M EARTHQUAKE</span> HITS NEPAL</div>
        <div class="flick-h2"><h3>8.1M Affected People. 2.8M People Displaced. 3.5M People in need of food assistance. Over 6000 deaths. Over 10000 Injured.</h3></div>
        
        <div class="learn"><a href="donate" target="_blank">Donate Now</a></div>
      </li>
            <li data-background="<?php echo base_url('assets/img/nepal_slider2.jpg') ?>">
        <div class="flick-title">NEPAL NEEDS <span class="color">YOUR</span> HELP</div>
        <div class="flick-h2"><h3>Every penny counts. Every prayer counts.</h3></div>
        
        <div class="learn"><a href="donate" target="_blank">Donate Now</a></div>
      </li>
      <li data-background="<?php echo base_url('assets/img/nepal_slider5.jpg') ?>">
        <div class="flick-title">LETS <span class="color">REBUILD</span> NEPAL</div>
        <div class="flick-h2"><h3>It can happen to us. It can happen to anyone.</h3></div>
        
        <div class="learn"><a href="donate" target="_blank">Donate Now</a></div>
      </li>
      
      
      
    </ul>
    
  </div>
  
   <!--===========================================time counter section==============================================--> 
     <section class="time-counter clearfix">
        <div class="container">
                <div class="upcoming pull-left clearfix">
                    <i class="fa fa-calendar pull-left"></i>
                    <div class="all-events pull-left">
                      <h3>RECENT EVENT</h3>
                        <h3 class="school">Pray for Nepal - Candlelight Vigil</h3>
                        <h4 class="school">7:30PM May 2nd, 2015. 300 Alamo Plaza, San Antonio, TX 78205</h4>
                        <a href="events">View All Events<span><i class="fa fa-long-arrow-right"></i></span></a>
                    </div>
                </div>
                <div class="timecount pull-right">
                 <div id="counter">
                        </div>
                </div>
        </div>
     </section>
<?php include('footer.php'); ?>
