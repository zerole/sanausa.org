<?php include('header.php'); ?>

<!--===========================================upcoming event events==============================================-->
      <section class="staff-heading">
        <div class="container clearfix">
          <div class="pull-left">
              <h4>Meetings</h4>
            </div>
            <div class="pull-right">
              <a href="">Home <i class="fa fa-angle-double-right"></i></a>
                <span>Meetings</span>
            </div>
        
        </div>
    </section>
	
	<!--===========================================Meeting Minutes August 3 2015==============================================--> 
     <section class="even">
     
      <div class="container">
          <div class="span1"></div>
          <div class="span10 pull-left">
          <p><h3>Meeting Minutes - August 3,2015</h3></p>
          <p>1.	Nishant dai has to provide Certificate of EIN to Anu di.</p>
          <p>2.	SANA Financial plan has been formulated by Vivek – Anu di will find out if that can be attached directly or needs to be filled out in the form.</p>
          <p>3.	Anu di will find out if bylaws need to be corporate or simple, and put together the same accordingly.</p>
		  <p>4.	Nelisha and Manasha will proofread materials when ready</p>
		  <p>5.	Paras bro will design certificate and add Vivek’s name in the certificate.</p>
		  <p>6.	Paras bro will figure out SANA email by next meeting.</p>
		  <p>7.	Next meeting has been set for Monday, August 10. The main agenda for the upcoming meeting will be discussions for Dashain/Tihar event.</p>
          </div>      
          <div class="span1"></div>
</div>
    
    
    </section>

    
    </section>

<?php include('footer.php'); ?>
