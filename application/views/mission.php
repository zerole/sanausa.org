<?php include('header.php'); ?>
<!--===========================================upcoming event events==============================================-->
      <section class="staff-heading">
        <div class="container clearfix">
          <div class="pull-left">
              <h4>Mission</h4>
            </div>
            <div class="pull-right">
              <a href="/">Home <i class="fa fa-angle-double-right"></i></a>
                <span>Mission</span>
            </div>
        
        </div>
      
      
      </section>

<!--===========================================upcoming events==============================================--> 
     <section class="even">
     
      <div class="container">
          <div class="span1"></div>
     <div class="span10 pull-left">    
<h4>Vision</h4>
<p>Our vision is to empower the community members with the understanding of their rights and social responsibility while serving the social, cultural and civic needs of the Nepali community and friends.<p>
<h4>Mission</h4>
<p>Our mission is to preserve, share and promote Nepali culture and heritage and provide a common venue for development for families and friends of Nepalese in the US. </p>
<h4>Objectives</h4>
<p>
  <ul>
    <li>1. To work in partnership with community organizations, local and state agencies for the well being of its members and community as a whole.</li>
    <li>2. To stage the beauty, the values and the virtues of Nepal and educate the younger generation to understand and foster their roots</li>
    <li>3. Establish a forum for community activities and sponsor the celebration of Nepali festivals and religious ceremonies</li>
    <li>4. To use this venue to hold fundraising events and programs to generate revenue for operating and capital funds.</li>
  </ul>
</p>
</div>
<div class="span1"></div>    
</div>
    
    
    </section>
<?php include('footer.php'); ?>
