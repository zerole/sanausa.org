<?php include('header.php'); ?>

<!--===========================================upcoming event events==============================================-->
      <section class="staff-heading">
        <div class="container clearfix">
          <div class="pull-left">
              <h4>SANA IN THE NEWS</h4>
            </div>
            <div class="pull-right">
              <a href="">Home <i class="fa fa-angle-double-right"></i></a>
                <span>News</span>
            </div>
        
        </div>
    </section>

<!--===========================================upcoming events==============================================--> 
     <section class="even">
     
      <div class="container">
          <div class="span2"></div>
     <div class="span8 pull-left">    
<ul>
<li>
<p>
<a href="http://www.news4sanantonio.com/news/features/top-stories/stories/Nepalaese-Host-Candlelight-Vigil-for-Quake-Victims-125948.shtml" target="window">Nepalese Host CandlelightVigil for Quake</a> May 02, 2015, News4Sanantonio.com</p></li>
<li><p><a href="http://www.ksat.com/content/pns/ksat/news/2015/05/02/vigil-held-for-nepal-at-alamo-plaza.html" target="window">Vigil held for Nepal at Alamo Plaza</a> May 02, 2015, Ksat.com</p></li>
<li><p><a href="http://www.expressnews.com/news/local/article/Alamo-vigil-mourns-Nepal-victims-6238858.php" target="window">Alamo vigil mourns Nepal victims</a> May 02, 2015, ExpressNews.com</p></li>
<li><p><a href="http://www.ksat.com/content/pns/ksat/news/2015/04/30/san-antonio-woman-originally-from-nepal-loses-mother-in-earthqua.html" target="window">San Antonio woman originally from Nepal loses mother in earthquake</a> April 30, 2015, Ksat.com</p><li>
</ul>
</p>
</div>
<div class="span2"></div>    
</div>
    
    
    </section>

<?php include('footer.php'); ?>
