<?php include('header.php'); ?>
<!--===========================================upcoming event events==============================================-->
<section class="staff-heading">
  <div class="container clearfix">
    <div class="pull-left">
      <h4>Donors List</h4>
    </div>
    <div class="pull-right">
      <a href="">Home <i class="fa fa-angle-double-right"></i></a>
      <span>Donors</span>
    </div>

  </div>
</section>

<!--===========================================upcoming events==============================================--> 
<section class="even">

  <div class="container">
    <div class="span2"></div>
    <div class="span8 pull-left">    
      <p>This table is only used to track donations made through Paypal, and is updated instantaneously. Donations made through cash, checks or wire transfer are not tracked here but can be made available through email for confirmation and transparency. If you want to donate, <a href="donate">click here</a></p>
      <div class="panel panel-default">
        <table class="table">
          <thead>
          <tr><th>Date</th><td>Name</th><th>Amount</th></tr>
          </thead>
          <tbody>
             <?php foreach($transaction_list as $transaction) { ?>
            <tr><td><?php echo $transaction["timestamp"]; ?></td><td><?php echo $transaction["name"]; ?></td><td><?php echo $transaction["currency_code"]; ?> $<?php echo $transaction["amt"]; ?></td></tr>
            <?php } ?>
            <tr><td>May 03, 2015</td><td>Simon Castillo</td><td>USD $25</td></tr>
            <tr><td>May 02, 2015</td><td>Basu panthi</td><td>USD $50</td></tr>
            <tr><td>May 01, 2015</td><td>Komal Gurung</td><td>USD $51</td></tr>
            <tr><td>April 30, 2015</td><td>Nishant Hada</td><td>USD $10</td></tr>
            <tr><td>April 30, 2015</td><td>Georjina Cineros</td><td>USD $10</td></tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="span2"></div>    
  </div>


</section>
<?php include('footer.php'); ?>
