<?php
define ( 'DEFAULT_TITLE','San Antonio Nepalese Association :: SANA');
define ( 'DEFAULT_META_KEYWORDS', "San Antonio Nepali, Nepali in San Antonio, Nepal, Nepali community, Nepali students, Nepali people in San Antonio, Help Nepal, Pray for Nepal" );    
define ( 'DEFAULT_META_DESC', "Looking for Nepali in San Antonio? Join the San Antonio Nepalese Community. Celebrate Nepal. Promote Nepal. Cooperate with fellow Nepalis. Contribute back." ); ?>
